import { Component, OnInit } from '@angular/core';
import {DateService} from '../../../services/date.service';
import 'moment/locale/ru';

@Component({
  selector: 'app-date-control',
  templateUrl: './date-control.component.html',
  styleUrls: ['./date-control.component.scss']
})
export class DateControlComponent implements OnInit {

  public modalDateFiltering = false;

  constructor(public dateService: DateService) { }

  ngOnInit(): void {
  }

}
